package model

import "gitlab.com/hanafi.firdaus/go-gin-codebase-modules/entity"

// Header is
type Header struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Responses is
type Responses struct {
	Code      int         `json:"code"`
	Status    string      `json:"status"`
	Message   string      `json:"message"`
	ErrorCode int         `json:"error_code"`
	Data      interface{} `json:"data"`
}

type FormData struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type ModifyUsers struct {
	entity.Users
	IsActive bool
}

type CaptureError struct {
	Type       string `json:"type"`
	HttpCode   int    `json:"http_code"`
	ErrorCode  int    `json:"error_code"`
	ResultCode string `json:"resultCode"`
}

// type Responses struct {
// 	Data interface{} `json:"data"`
// }
