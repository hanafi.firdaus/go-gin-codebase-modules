package entity

import (
	"database/sql"
)

type User struct {
	ID        int            `json:"id"`
	Fullname  sql.NullString `json:"fullname,omitempty"`
	NoHp      sql.NullString `json:"no_hp"`
	IsAttend  sql.NullBool   `json:"is_attend"`
	CreatedAt sql.NullTime   `json:"created_at"`
}
