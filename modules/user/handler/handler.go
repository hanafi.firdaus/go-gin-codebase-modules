package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"

	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/middleware"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/interfaces"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type Handler struct {
	Usecase   interfaces.UsecaseUser
	Rds       *redis.Client
	Logger    *logrus.Logger
	Res       *helper.Responses
	Validator *validator.Validate
}

func CreateHandler(Usecase interfaces.UsecaseUser, rds *redis.Client, logger *logrus.Logger, res *helper.Responses, validator *validator.Validate) *Handler {
	return &Handler{Usecase, rds, logger, res, validator}
}

func (a *Handler) GetInfoUser(c *gin.Context) {
	var User middleware.EntityVerifikasiToken
	bind, ok := c.MustGet("bind").([]byte)
	if !ok {
		a.Res.JsonWithErrorCode(c, http.StatusBadRequest, helper.ErrorKetikaMendapatkanDataUser)
		return
	}

	json.Unmarshal(bind, &User)
	a.Res.Json(c, http.StatusOK, User.Data, "testing")
}

func (a *Handler) Hello(c *gin.Context) {
	time.Sleep(time.Second * 5)

	a.Res.Json(c, http.StatusOK, map[string]interface{}{
		"message": "Hello World",
	}, "testing")
}

func (a *Handler) GetOneUser(c *gin.Context) {
	responses, err := a.Usecase.GetOneUser(c.Request.Context())
	if err != nil {
		a.Res.Json(c, http.StatusInternalServerError, map[string]interface{}{
			"Error": err.Error(),
		}, err.Error())
		return
	}

	a.Res.Json(c, http.StatusOK, responses, "Success")
}

func (a *Handler) CreateOneUser(c *gin.Context) {
	var request model.RequestCreateOneUser
	if err := c.ShouldBindJSON(&request); err != nil {
		a.Res.Json(c, http.StatusBadRequest, map[string]interface{}{
			"error": err.Error(),
		}, err.Error())
		return
	}

	err := a.Validator.Struct(request)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			a.Logger.Error(err.Error())
			a.Res.Json(c, http.StatusBadRequest, nil, err.Error())
			return
		}

		for _, err := range err.(validator.ValidationErrors) {
			a.Res.Json(c, http.StatusBadRequest, nil, err.Field()+" "+err.Tag())
			return
		}
	}

	res, err := a.Usecase.CreateOneUser(c.Request.Context(), request)
	if err != nil {
		a.Logger.Error(err.Error())
		a.Res.Json(c, http.StatusBadRequest, nil, err.Error())
		return
	}

	a.Res.Json(c, http.StatusCreated, res, "Success")
}

func (a *Handler) CaptureErrorExample(c *gin.Context) {
	err := a.Usecase.CaptureErrorExample()
	if err != nil {
		a.Res.JsonWithCaptureError(c, err)
		return
	}
	a.Res.Json(c, http.StatusOK, nil, "Oks")
}

func (a *Handler) GetAllUsers(c *gin.Context) {
	res, err := a.Usecase.GetAllUsers(c.Request.Context())
	if err != nil {
		a.Res.Json(c, http.StatusInternalServerError, nil, err.Error())
		return
	}

	a.Res.Json(c, http.StatusOK, res, "Success")
}
