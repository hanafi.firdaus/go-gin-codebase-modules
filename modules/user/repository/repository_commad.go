package repository

import (
	"context"

	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
)

func (r *RepositoryUserImpl) CreateOneUser(ctx context.Context, user entity.User) (returnUser entity.User, err error) {
	var id int = 0
	query := `INSERT INTO sharing_session.users (fullname, no_hp, is_attend) VALUES($1, $2, $3) RETURNING id`
	err = r.DBWrite.QueryRowContext(ctx, query, user.Fullname.String, user.NoHp.String, user.IsAttend.Bool).Scan(&id)
	if err != nil {
		r.logger.Error(err.Error())
		return
	}
	returnUser = user
	returnUser.ID = id
	return
}
