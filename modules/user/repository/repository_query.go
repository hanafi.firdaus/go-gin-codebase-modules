package repository

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/interfaces"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"
)

type RepositoryUserImpl struct {
	// CQRS design pattern best practice. memisahkan DB untuk write dan read
	DBWrite *sql.DB
	DBRead  *sql.DB
	logger  *logrus.Logger
}

func CreateRepository(dbWrite, dbRead *sql.DB, logger *logrus.Logger) interfaces.RepositoryUser {
	return &RepositoryUserImpl{dbWrite, dbRead, logger}
}

func (r *RepositoryUserImpl) GetOneUser(ctx context.Context) (user entity.User, err error) {
	query := "select u.id, u.fullname, u.no_hp, u.is_attend, u.created_at from sharing_session.users u limit 1"
	entityUser := entity.User{}
	err = r.DBRead.QueryRowContext(ctx, query).
		Scan(&entityUser.ID, &entityUser.Fullname, &entityUser.NoHp, &entityUser.IsAttend, &entityUser.CreatedAt)
	if err != nil {
		return
	}
	user = entityUser
	return user, nil
}

func (r *RepositoryUserImpl) GetAllUsers(ctx context.Context) (user []model.ResponsesGetAllUser, err error) {
	query := "select u.id, u.fullname, u.no_hp, u.is_attend, u.created_at from sharing_session.users u"
	rows, errQuery := r.DBRead.QueryContext(ctx, query)
	if errQuery != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		entityUser := entity.User{}
		err = rows.Scan(&entityUser.ID, &entityUser.Fullname, &entityUser.NoHp, &entityUser.IsAttend, &entityUser.CreatedAt)
		if err != nil {
			return
		}
		user = append(user, model.ResponsesGetAllUser{
			ID:        entityUser.ID,
			Fullname:  entityUser.Fullname.String,
			NoHp:      entityUser.NoHp.String,
			IsAttend:  entityUser.IsAttend.Bool,
			CreatedAt: entityUser.CreatedAt.Time.Format("2006-01-02 15:04:05"),
		})
	}
	return
}
