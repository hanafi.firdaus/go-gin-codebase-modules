package initiation

import (
	"database/sql"
	"os"

	config "gitlab.com/hanafi.firdaus/go-gin-codebase-modules/config"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/middleware"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/handler"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/repository"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/usecase"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

func Init(routesGin *gin.Engine, logger *logrus.Logger) (*gin.Engine, *sql.DB, *redis.Client) {
	pq := config.ApiGateway(logger)
	redis := config.Redis(logger)

	validator := validator.New()
	repo := repository.CreateRepository(pq, pq, logger)

	usecase := usecase.CreateUsecase(repo, logger)
	response := helper.CreateCustomResponses(os.Getenv("DOMAIN_NAME"))
	handler := handler.CreateHandler(usecase, redis, logger, response, validator)

	middle := middleware.Init(logger, response)
	routesGin = user.Routes(routesGin, handler, middle)

	return routesGin, pq, redis
}
