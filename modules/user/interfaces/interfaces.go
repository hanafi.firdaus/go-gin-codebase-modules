package interfaces

import (
	"context"

	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"
)

type RepositoryUser interface {
	GetOneUser(ctx context.Context) (user entity.User, err error)
	CreateOneUser(ctx context.Context, user entity.User) (returnUser entity.User, err error)
	GetAllUsers(ctx context.Context) (user []model.ResponsesGetAllUser, err error)
}

type UsecaseUser interface {
	GetOneUser(ctx context.Context) (responses model.ResponsesGetOneUser, err error)
	CreateOneUser(ctx context.Context, user model.RequestCreateOneUser) (responses model.ResponsesGetOneUser, err error)
	CaptureErrorExample() (err error)
	GetAllUsers(ctx context.Context) (user []model.ResponsesGetAllUser, err error)
}
