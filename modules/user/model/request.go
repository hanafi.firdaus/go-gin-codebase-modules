package model

type RequestCreateOneUser struct {
	Fullname string `json:"fullname" validate:"required"`
	NoHp     string `json:"no_hp" validate:"required"`
	IsAttend bool   `json:"is_attend"`
}
