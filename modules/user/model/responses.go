package model

type ResponsesGetOneUser struct {
	ID        int    `json:"id"`
	Fullname  string `json:"fullname"`
	NoHp      string `json:"no_hp"`
	IsAttend  bool   `json:"is_attend"`
	CreatedAt string `json:"created_at"`
}

type ResponsesGetAllUser struct {
	ID        int    `json:"id"`
	Fullname  string `json:"fullname"`
	NoHp      string `json:"no_hp"`
	IsAttend  bool   `json:"is_attend"`
	CreatedAt string `json:"created_at"`
}
