package user

import (
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/middleware"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/handler"

	"github.com/gin-gonic/gin"
)

func Routes(routesGin *gin.Engine, hand *handler.Handler, middle middleware.UsecaseMiddleware) *gin.Engine {
	jwtAuth := routesGin.Group("/v1/api")

	// api with verifikasi jwt token
	jwtAuth.GET("/hello", hand.Hello)
	jwtAuth.GET("/capture-error-example", hand.CaptureErrorExample)
	jwtAuth.GET("/get-all-user", hand.GetAllUsers)

	jwtAuth.Use(middle.VerifyAutorizationToken()) // Verify Authorization
	{
		jwtAuth.GET("/oneuser", hand.GetOneUser)
		jwtAuth.POST("/create-one-user", hand.CreateOneUser)
		jwtAuth.GET("/infouser", hand.GetInfoUser)
	}

	return routesGin
}
