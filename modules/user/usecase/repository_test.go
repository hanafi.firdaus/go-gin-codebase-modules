package usecase

import (
	"context"

	"github.com/stretchr/testify/mock"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/interfaces"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"
)

type MockRepositoryUserImpl struct {
	mock.Mock
}

func CreateMockRepository() interfaces.RepositoryUser {
	return &MockRepositoryUserImpl{}
}

// func MockCreateRepository(dbWrite, dbRead *sql.DB, logger *helper.CostumLogger) interfaces.RepositoryUser {
// 	return &RepositoryUserImpl{dbWrite, dbRead, logger}
// }

func (r *MockRepositoryUserImpl) CreateOneUser(ctx context.Context, user entity.User) (returnUser entity.User, err error) {
	args := r.Called(ctx, user)
	return args.Get(0).(entity.User), args.Error(1)
}

func (r *MockRepositoryUserImpl) GetAllUsers(ctx context.Context) (user []model.ResponsesGetAllUser, err error) {
	args := r.Called(ctx)
	return args.Get(0).([]model.ResponsesGetAllUser), args.Error(1)
}

func (r *MockRepositoryUserImpl) GetOneUser(ctx context.Context) (user entity.User, err error) {
	args := r.Called(ctx)
	return args.Get(0).(entity.User), args.Error(1)
}
