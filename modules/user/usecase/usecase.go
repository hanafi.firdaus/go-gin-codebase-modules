package usecase

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"
	gmodel "gitlab.com/hanafi.firdaus/go-gin-codebase-modules/model"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/interfaces"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"
)

// Usecase is
type UsecaseUserImpl struct {
	Repository interfaces.RepositoryUser
	Logger     *logrus.Logger
}

// CreateUsecase is
func CreateUsecase(repo interfaces.RepositoryUser, logger *logrus.Logger) interfaces.UsecaseUser {
	return &UsecaseUserImpl{
		Repository: repo,
		Logger:     logger,
	}
}

func (u *UsecaseUserImpl) GetOneUser(ctx context.Context) (responses model.ResponsesGetOneUser, err error) {
	user, err := u.Repository.GetOneUser(ctx)
	if err != nil {
		return
	}
	responses = model.ResponsesGetOneUser{
		ID:        user.ID,
		Fullname:  user.Fullname.String,
		NoHp:      user.NoHp.String,
		IsAttend:  user.IsAttend.Bool,
		CreatedAt: user.CreatedAt.Time.Format("2006-01-02 15:04:05"),
	}
	return
}

func (u *UsecaseUserImpl) CaptureErrorExample() (err error) {
	err = helper.SetCaptureError(gmodel.CaptureError{
		HttpCode:  404,
		ErrorCode: helper.IniErrorYangBaru,
	})
	return
}

func (u *UsecaseUserImpl) CreateOneUser(ctx context.Context, user model.RequestCreateOneUser) (responses model.ResponsesGetOneUser, err error) {
	res, err := u.Repository.CreateOneUser(ctx, entity.User{
		Fullname: sql.NullString{String: user.Fullname, Valid: true},
		NoHp:     sql.NullString{String: user.NoHp, Valid: true},
		IsAttend: sql.NullBool{Bool: user.IsAttend, Valid: true},
	})
	if err != nil {
		u.Logger.Error(err.Error())
		return
	}
	responses = model.ResponsesGetOneUser{
		ID:        res.ID,
		Fullname:  res.Fullname.String,
		NoHp:      res.NoHp.String,
		CreatedAt: res.CreatedAt.Time.Format("2006-01-02 15:04:05"),
		IsAttend:  res.IsAttend.Bool,
	}
	return
}

func (u *UsecaseUserImpl) GetAllUsers(ctx context.Context) (user []model.ResponsesGetAllUser, err error) {
	return u.Repository.GetAllUsers(ctx)
}
