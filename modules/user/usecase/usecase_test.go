package usecase

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"
	gmodel "gitlab.com/hanafi.firdaus/go-gin-codebase-modules/model"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/entity"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/modules/user/model"
)

type TestUsecase struct {
	Mock   *MockRepositoryUserImpl
	Logger *logrus.Logger
	Ctx    context.Context
}

func CreateInitiationTest() *TestUsecase {
	var logger = logrus.New()
	logger.SetReportCaller(true)
	return &TestUsecase{Mock: new(MockRepositoryUserImpl), Logger: logger, Ctx: context.Background()}
}

func TestCreateOneUser(t *testing.T) {

	u := CreateInitiationTest()

	user := entity.User{
		Fullname: sql.NullString{String: "Hanafi", Valid: true},
		NoHp:     sql.NullString{String: "123", Valid: true},
		IsAttend: sql.NullBool{Bool: true, Valid: true},
	}

	u.Mock.On("CreateOneUser", u.Ctx, user).Return(user, nil)

	usecase := CreateUsecase(u.Mock, u.Logger)
	resp, err := usecase.CreateOneUser(u.Ctx, model.RequestCreateOneUser{
		Fullname: "Hanafi",
		NoHp:     "123",
		IsAttend: true,
	})

	assert.Equal(t, err, nil)

	assert.Equal(t, resp, model.ResponsesGetOneUser{
		Fullname:  "Hanafi",
		NoHp:      "123",
		IsAttend:  true,
		CreatedAt: "0001-01-01 00:00:00",
	})
}

func TestCreateOneUserErrDB(t *testing.T) {
	u := CreateInitiationTest()

	user := entity.User{
		Fullname: sql.NullString{String: "Hanafi", Valid: true},
		NoHp:     sql.NullString{String: "123", Valid: true},
		IsAttend: sql.NullBool{Bool: true, Valid: true},
	}

	u.Mock.On("CreateOneUser", u.Ctx, user).Return(user, fmt.Errorf("error while create user"))

	usecase := CreateUsecase(u.Mock, u.Logger)
	_, err := usecase.CreateOneUser(u.Ctx, model.RequestCreateOneUser{
		Fullname: "Hanafi",
		NoHp:     "123",
		IsAttend: true,
	})

	log.Println(err)

	assert.Equal(t, err.Error(), "error while create user")
}

func TestGetAllUsers(t *testing.T) {
	u := CreateInitiationTest()
	var tms time.Time = time.Now()
	users := []model.ResponsesGetAllUser{
		{
			ID:        1,
			Fullname:  "Hanafi",
			NoHp:      "123",
			IsAttend:  true,
			CreatedAt: tms.Local().Format("2006-01-02 15:04:05"),
		}, {
			ID:        2,
			Fullname:  "Firdaus",
			NoHp:      "123456",
			IsAttend:  true,
			CreatedAt: tms.Local().Format("2006-01-02 15:04:05"),
		},
	}

	u.Mock.On("GetAllUsers", u.Ctx).Return(users, nil)

	usecase := CreateUsecase(u.Mock, u.Logger)
	resp, _ := usecase.GetAllUsers(u.Ctx)
	assert.Equal(t, resp, users)
}

func TestGetOneUser(t *testing.T) {
	u := CreateInitiationTest()
	var tms time.Time = time.Now()
	user := model.ResponsesGetOneUser{
		ID:        1,
		Fullname:  "Hanafi",
		NoHp:      "123",
		IsAttend:  true,
		CreatedAt: tms.Local().Format("2006-01-02 15:04:05"),
	}

	entityUser := entity.User{
		ID:        1,
		Fullname:  sql.NullString{String: "Hanafi", Valid: true},
		NoHp:      sql.NullString{String: "123", Valid: true},
		IsAttend:  sql.NullBool{Bool: true, Valid: true},
		CreatedAt: sql.NullTime{Time: tms, Valid: true},
	}

	u.Mock.On("GetOneUser", u.Ctx).Return(entityUser, nil)
	usecase := CreateUsecase(u.Mock, u.Logger)
	resp, _ := usecase.GetOneUser(u.Ctx)
	assert.Equal(t, resp, user)
}

func TestErrorGetOneUser(t *testing.T) {
	u := CreateInitiationTest()
	var tms time.Time = time.Now()
	entityUser := entity.User{
		ID:        1,
		Fullname:  sql.NullString{String: "Hanafi", Valid: true},
		NoHp:      sql.NullString{String: "123", Valid: true},
		IsAttend:  sql.NullBool{Bool: true, Valid: true},
		CreatedAt: sql.NullTime{Time: tms, Valid: true},
	}

	u.Mock.On("GetOneUser", u.Ctx).Return(entityUser, fmt.Errorf("user not found"))
	usecase := CreateUsecase(u.Mock, u.Logger)
	_, err := usecase.GetOneUser(u.Ctx)
	if err != nil {
		assert.Equal(t, err.Error(), "user not found")
	}
}

func TestCaptureErrorExample(t *testing.T) {
	u := CreateInitiationTest()
	usecase := CreateUsecase(u.Mock, u.Logger)
	err := usecase.CaptureErrorExample()
	assert.Equal(t, err, helper.SetCaptureError(gmodel.CaptureError{
		HttpCode:  404,
		ErrorCode: helper.IniErrorYangBaru,
	}))
}
