module gitlab.com/hanafi.firdaus/go-gin-codebase-modules

go 1.15

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.10.1 // indirect
	github.com/go-redis/redis/v8 v8.11.4
	github.com/joho/godotenv v1.4.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	go.elastic.co/apm v1.15.0
	go.elastic.co/apm/module/apmgoredisv8 v1.15.0
	go.elastic.co/apm/module/apmhttp v1.15.0
	go.elastic.co/apm/module/apmlogrus v1.15.0
	go.elastic.co/apm/module/apmsql v1.15.0
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064 // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	golang.org/x/tools v0.1.10 // indirect
)
