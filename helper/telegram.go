package helper

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/pkg/errors"
)

func SendErrorToTelegram(nameService, message string, err error) {

	type stackTracer interface {
		StackTrace() errors.StackTrace
	}

	errs, ok := errors.Cause(err).(stackTracer)
	if !ok {
		panic("oops, err does not implement stackTracer")
	}

	st := errs.StackTrace()
	// fmt.Printf("%+v", st[0:2]) // top two frames
	fmt.Printf("%+v", st[0:1]) // top two frames

	token := ""
	chatID := ""
	if os.Getenv("MODE") == "development" {
		token = os.Getenv("TOKEN_BOT_TELEGRAM_TESTING")
		chatID = os.Getenv("CHAT_ID_TESTING")
	} else {
		token = os.Getenv("TOKEN_BOT_TELEGRAM")
		chatID = os.Getenv("CHAT_ID")
	}

	owner := os.Getenv("DOMAIN_NAME")

	url := "https://api.telegram.org/" + token + "/sendMessage"
	method := "POST"

	text := "service: *" + nameService + "*\n" + "owner: *" + owner + "*\n" + "messages: *" + message + "*"

	payload := strings.NewReader("chat_id=" + chatID + "&text=" + text + "&parse_mode=Markdown")
	if strings.Contains(message, "*") {
		payload = strings.NewReader("chat_id=" + chatID + "&text=" + text)
	}

	client := &http.Client{}
	req, _ := http.NewRequest(method, url, payload)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		// http.Error(w, "failed to query backend", 500)
		return
	} else {
		defer res.Body.Close()
	}
}
