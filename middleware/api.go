package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/model"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type ApiImpl struct {
	Rds    *redis.Client
	Logger *logrus.Logger
}

func CreateApi(rds *redis.Client, logger *logrus.Logger) ApisMiddleware {
	return &ApiImpl{rds, logger}
}

func (a *ApiImpl) VerifikasiToken(ctx context.Context, token string) (codes int, vrf EntityVerifikasiToken, err error) {
	body, err := helper.CallAPI(ctx, a.Logger, os.Getenv("URL_USEETV")+os.Getenv("URL_VERIFIKASI_TOKEN_API_GATEWAY"), "POST", nil, []model.Header{
		{Key: "Authorization", Value: "Bearer " + token},
	})
	if err != nil {
		a.Logger.Error(err.Error())
		return
	}

	// Assign to interface model
	var result map[string]interface{}
	err = json.Unmarshal([]byte(body), &result)
	if err != nil {
		a.Logger.Error(err.Error())
		return
	}

	if code, ok := result["code"].(float64); ok {
		if code > 399 {
			if errorCode, ok := result["error_code"].(float64); ok {
				codes = int(errorCode)
			}
			err = fmt.Errorf(result["message"].(string))
			a.Logger.Error(err.Error())
			return
		}
	}

	err = json.Unmarshal([]byte(body), &vrf)
	if err != nil {
		a.Logger.Error(err.Error())
		return
	}

	return
}
