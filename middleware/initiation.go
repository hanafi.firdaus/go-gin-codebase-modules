package middleware

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/config"
	"gitlab.com/hanafi.firdaus/go-gin-codebase-modules/helper"
)

func Init(logger *logrus.Logger, res *helper.Responses) UsecaseMiddleware {
	redis := config.Redis(logger)

	apis := CreateApi(redis, logger)

	return CreateUsecaseMiddleware(logger, redis, apis, res)
}
